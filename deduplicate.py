#!/usr/bin/env python3
# encoding: utf-8

import argparse
import hashlib
import logging
import os


def primary(files, pri):
    """@todo: Docstring for primary.

    :arg1: @todo
    :returns: @todo

    """
    folders = []
    filesList = []
    
    for entry in os.scandir(pri):
        if entry.is_dir():
            folders.append(entry.path)
            primary(files, entry)
        elif entry.is_file():
            fileData = []
            fileData.append(os.path.getsize(entry.path))
            fileData.append(os.path.basename(entry.path))
            files[hashlib.md5(open(entry.path, 'rb').read()).hexdigest()] = list(fileData)
            filesList.append(entry.path)
 
    #print('Folders:')
    #print(folders)
    #print('Files:')
    #print(filesList)

def secondary(files, sec, delete=False, deleteEmptyDirs=False, aggressive=False):
    """@todo: Docstring for secondary.

    :arg1: @todo
    :returns: @todo

    """
    folders = []
    filesList = []
    
    for entry in os.scandir(sec):
        if entry.is_dir():
            folders.append(entry.path)
            secondary(files, entry, delete, deleteEmptyDirs, aggressive)

            if os.listdir(entry.path) == []:
                logging.info("Deleting empty directory " + entry.path)
                if deleteEmptyDirs is True:
                    os.rmdir(entry.path)

        elif entry.is_file():
            fileData = []
            fileData.append(os.path.getsize(entry.path))
            fileData.append(os.path.basename(entry.path))

            fileHash = hashlib.md5(open(entry.path, 'rb').read()).hexdigest()

            # check if file is the same if hash is in the file dict
            if fileHash in files:
                # compare file sizes
                if files[fileHash][0] == fileData[0]:
                    # compare filenames
                    if files[fileHash][1] == fileData[1]:
                        if delete:
                            logging.info("Deleting file " + entry.path)
                            os.remove(entry)
                        else:
                            logging.info("Would have deleted file " + entry.path)
                    else:
                        logging.info("Hash collision: " + fileHash + " " + files[fileHash][1] + " and " + entry.path)
                        if aggressive:
                            logging.info("Deleting file " + entry.path)
                            os.remove(entry)
                        else:
                            logging.info("Would have deleted file " + entry.path)

            filesList.append(entry.path)
 
    #print('Folders:')
    #print(folders)
    #print('Files:')
    #print(filesList)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-p", "--primary",
        dest = "primaryDir",
        default = "",
        action = "store",
        help = "Root directory that is treated as sacred, will never have files deleted\n"
        )

    parser.add_argument(
        "-s", "--secondary",
        dest = "secondaryDir",
        default = "",
        action = "store",
        help = "Directory where files will be deleted if they are duplicates\n"
        )

    parser.add_argument(
        "-d", "--delete",
        dest = "delete",
        default = "false",
        action = "store_true",
        help = "Delete duplicate files\n"
        )

    parser.add_argument(
        "--del-dir",
        dest = "deleteEmptyDirs",
        default = "false",
        action = "store_true",
        help = "Delete directories that are empty\n"
        )

    parser.add_argument(
        "--aggressive",
        dest = "aggressive",
        default = "false",
        action = "store_true",
        help = "Delete otherwise identical files that have different names\n"
        )

    args = parser.parse_args()

    print(args)
    logging.basicConfig(filename="sample.log", filemode="w", level=logging.INFO)
    
    #logging.debug("This is a debug message")
    logging.info("Informational message")
    #logging.error("An error has happened!")

    files = {}
    primary(files,args.primaryDir)
    secondary(files, args.secondaryDir, args.delete, args.deleteEmptyDirs, args.aggressive)

